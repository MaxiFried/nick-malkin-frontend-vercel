export default function BandcampIframe({ src, href }) {
    return (
        <div className='bandcamp-wrapper'>
            <div className='bandcamp'>
                <iframe style={{ border: '0', width: '350px', height: '470px' }} src={src} seamless>
                    <a href={href}>Interrupted Verse by Nick Malkin</a>
                </iframe>
            </div>
        </div>
    );
}
