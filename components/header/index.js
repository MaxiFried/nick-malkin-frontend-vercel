import Link from 'next/link';
import { useState } from 'react';

export default function Header({ projects = [], musics = [] }) {
    console.log(musics);
    // const [showProjects, setShowProjects] = useState(false);
    // const [showMusic, setShowMusic] = useState(false);
    // const handleClick = (menu) => {
    //     if (menu === 'projects') {
    //         setShowProjects(true);
    //         setShowMusic(false);
    //     } else if (menu === 'music') {
    //         setShowProjects(false);
    //         setShowMusic(true);
    //     } else {
    //         setShowProjects(false);
    //         setShowMusic(false);
    //     }
    // };

    return (
        <div>
            {/* loop over prjects and show them  */}
            <h1>nick malkin</h1>
            <h2>music</h2>
            {musics &&
                musics.map((music) => (
                    <Link href={`/project/${music.slug}`} key={music.id}>
                        <h3>{music.title}</h3>
                    </Link>
                ))}
            <h2>projects</h2>
            {projects &&
                projects.map((project) => (
                    <Link href={`/project/${project.slug}`} key={project.id}>
                        <h3>{project.title}</h3>
                    </Link>
                ))}
            <h2>miscellaneous</h2>
        </div>
    );
}
