import styles from '../styles/Home.module.css';
import Link from 'next/link';
import Header from '../components/header';

export default function Home({ projectsJSON, musicJSON }) {
    return <Header projects={projectsJSON} musics={musicJSON} />;
}

export async function getStaticProps() {
    //get projects from api
    const projects = await fetch('http://85.214.103.19/projects');
    const projectsJSON = await projects.json();
    //get music from api
    const music = await fetch('http://85.214.103.19/sounds');
    const musicJSON = await music.json();
    return {
        props: { projectsJSON, musicJSON },
    };
}
