import dynamic from 'next/dynamic';
import Link from 'next/link';
import Header from '../../components/header';
import BandcampIframe from '../../components/bandcampIframe';

export default function Project({ project, projects }) {
    const DynamicComponent = dynamic(() => import('../../components/bandcampIframe'));

    // const DynamicComponent = dynamic(() => import('../../components/bandcampIframe').then((mod) => mod.BandcampIframe));
    // const replaceContent = (data) => {
    //     console.log(data);
    //     let content = data.replace(/href/g, "target='_blank' href");
    //     content = content.replace(/src="/g, `src="http://85.214.103.19`);
    //     return content;
    // };
    return (
        <>
            <div className='row'>
                <div className='col-6'>
                    <Header projects={projects} />
                </div>
                <div className='col-6'>
                    <div>{project.description}</div>
                    <DynamicComponent src='https://bandcamp.com/EmbeddedPlayer/album=1228589089/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/transparent=true/' href='https://nickmalkin.bandcamp.com/album/interrupted-verse' />
                    <div
                        dangerouslySetInnerHTML={{
                            __html: project.content ? project.content : '',
                        }}
                    />
                </div>
            </div>
        </>
    );
}

export async function getStaticPaths() {
    const res = await fetch(`http://85.214.103.19/projects`);
    const projects = await res.json();

    const paths = projects.map((project) => ({
        params: { slug: project.slug },
    }));

    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const { slug } = params;

    const res = await fetch(`http://85.214.103.19/projects?slug=${slug}`);
    const res1 = await fetch(`http://85.214.103.19/projects`);
    const data = await res.json();
    const data1 = await res1.json();

    const project = data[0];
    const projects = data1;
    return {
        props: { project, projects },
    };
}
